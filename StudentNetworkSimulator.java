/**
 * Author: David Onken
 * 
 * WARNING: I have modified NetworkSimulator.java. This program does
 * not function as intended without my change.
 * 
 * Justification: NetworkSimulator would quit as soon as it wrote all
 * messages to the student, which meant that any in-progress ACKs and 
 * writes would be prematurely cut. I moved the nSim >= maxMessages IF
 * statement to the FROMLAYER5 case of the switch to prevent this premature
 * cut out and ensure delivery of all Layer5 data before quitting.
 */


import java.util.LinkedList;

public class StudentNetworkSimulator extends NetworkSimulator
{
    /*
     * Predefined Constants (static member variables):
     *
     *   int MAXDATASIZE : the maximum size of the Message data and
     *                     Packet payload
     *
     *   int A           : a predefined integer that represents entity A
     *   int B           : a predefined integer that represents entity B
     *
     *
     * Predefined Member Methods:
     *
     *  void stopTimer(int entity): 
     *       Stops the timer running at "entity" [A or B]
     *  void startTimer(int entity, double increment): 
     *       Starts a timer running at "entity" [A or B], which will expire in
     *       "increment" time units, causing the interrupt handler to be
     *       called.  You should only call this with A.
     *  void toLayer3(int callingEntity, Packet p)
     *       Puts the packet "p" into the network from "callingEntity" [A or B]
     *  void toLayer5(int entity, String dataSent)
     *       Passes "dataSent" up to layer 5 from "entity" [A or B]
     *  double getTime()
     *       Returns the current time in the simulator.  Might be useful for
     *       debugging.
     *  void printEventList()
     *       Prints the current event list to stdout.  Might be useful for
     *       debugging, but probably not.
     *
     *
     *  Predefined Classes:
     *
     *  Message: Used to encapsulate a message coming from layer 5
     *    Constructor:
     *      Message(String inputData): 
     *          creates a new Message containing "inputData"
     *    Methods:
     *      boolean setData(String inputData):
     *          sets an existing Message's data to "inputData"
     *          returns true on success, false otherwise
     *      String getData():
     *          returns the data contained in the message
     *  Packet: Used to encapsulate a packet
     *    Constructors:
     *      Packet (Packet p):
     *          creates a new Packet that is a copy of "p"
     *      Packet (int seq, int ack, int check, String newPayload)
     *          creates a new Packet with a sequence field of "seq", an
     *          ack field of "ack", a checksum field of "check", and a
     *          payload of "newPayload"
     *      Packet (int seq, int ack, int check)
     *          chreate a new Packet with a sequence field of "seq", an
     *          ack field of "ack", a checksum field of "check", and
     *          an empty payload
     *    Methods:
     *      boolean setSeqnum(int n)
     *          sets the Packet's sequence field to "n"
     *          returns true on success, false otherwise
     *      boolean setAcknum(int n)
     *          sets the Packet's ack field to "n"
     *          returns true on success, false otherwise
     *      boolean setChecksum(int n)
     *          sets the Packet's checksum to "n"
     *          returns true on success, false otherwise
     *      boolean setPayload(String newPayload)
     *          sets the Packet's payload to "newPayload"
     *          returns true on success, false otherwise
     *      int getSeqnum()
     *          returns the contents of the Packet's sequence field
     *      int getAcknum()
     *          returns the contents of the Packet's ack field
     *      int getChecksum()
     *          returns the checksum of the Packet
     *      int getPayload()
     *          returns the Packet's payload
     *
     */

    // Add any necessary class variables here.  Remember, you cannot use
    // these variables to send messages error free!  They can only hold
    // state information for A or B.
    // Also add any necessary methods (e.g. checksum of a String)
	
    //boolean a_waiting;
    LinkedList<Message> message_queue;
    
	int a_req_num;
	int a_seq_num;
	int b_req_num;
	int a_seq_base;
	int a_seq_max;
	LinkedList<Message> window;
    
    public long successful_receives;
    public long failed_receives;
    public long sends;
    public long timeouts;
    public long receives;
    

    // This is the constructor.  Don't touch!
    public StudentNetworkSimulator(int numMessages,
                                   double loss,
                                   double corrupt,
                                   double avgDelay,
                                   int trace,
                                   long seed)
    {
        super(numMessages, loss, corrupt, avgDelay, trace, seed);
    }
    
    public void outputStats() {
    	System.out.println("Statistics: A Sends - " 
    			+ sends + " | A Receives - " 
    			+ receives + " | A Timeouts - " 
    			+ timeouts + " | B Successful Receives - " 
    			+ successful_receives + " | B Failed Receives - " 
    			+ failed_receives);
    }
    

    // This routine will be called whenever the upper layer at the sender [A]
    // has a message to send.  It is the job of your protocol to insure that
    // the data in such a message is delivered in-order, and correctly, to
    // the receiving upper layer.
    protected void aOutput(Message message)
    {
    	System.out.println("A: Output request: " + message.getData());
    	
    	// If the A side is waiting on an ACK, add the message to a waiting queue and continue
    	if (window.size() <= 7)
    	{
    		
    		if (a_seq_num <= a_seq_max)
    		{
    			sends++;
        		System.out.println("A: Sending " + message.getData() + " With Seq #" + a_seq_num);
    	    	Packet sndpkt = new Packet(a_seq_num, 
    	    			a_seq_num + 1, 
						(message.getData() + a_seq_num + (a_seq_num + 1)).hashCode(), 
    					message.getData());
    	    	toLayer3(0, sndpkt);
    	    	
    	    	// Add message to current window
    	    	window.add(message);
    	    	
    	    	// If this the first packet in the window, start the clock.
    	    	if (a_seq_num == a_seq_base)
    	    	{
    	    		startTimer(0, 30*8);
    	    	}
    	    	// Increment the sequence number
    	    	a_seq_num++;
    		} else {
        		System.out.println("A: Rejected, waiting on ACK");
        		message_queue.add(message);
        	}
    	} else {
    		System.out.println("A: Rejected, waiting on ACK");
    		message_queue.add(message);
    	}
    }
    
    // This routine will be called whenever a packet sent from the B-side 
    // (i.e. as a result of a toLayer3() being done by a B-side procedure)
    // arrives at the A-side.  "packet" is the (possibly corrupted) packet
    // sent from the B-side.
    protected void aInput(Packet packet)
    {
    	System.out.print("A: Incoming ACK Packet: ");
    	
		if (packet.getChecksum() == (packet.getPayload() + packet.getSeqnum() + packet.getAcknum()).hashCode() 
    			&& packet.getAcknum() > a_seq_base)
    	{
			// Stop timer
			stopTimer(0);
			receives++;
			
			// Figure out how many ACKs we "missed" and shorten table
			int diff = packet.getAcknum() - a_seq_base;
			
			// Reset sender numbers
    		a_seq_max += (packet.getAcknum() - a_seq_base);
    		a_seq_base = packet.getAcknum();
    		
    		// Remove from window
    		for (int i = 0; i < diff; i++)
    		{
    			window.remove();
    			
    			// If there are messages that were queued, send them now.
        		// This functions such that the queue will empty before
        		// it executes properly.
        		if (message_queue.size() > 0)
        		{
        			aOutput(message_queue.remove());
        		}
    		}
    		System.out.println("Accepted ACK # " + packet.getAcknum());
    		
    		// If the window has items remaining, start a new clock
    		if (!window.isEmpty())
    		{
    			startTimer(0, 30*8);
    		}
    		
    	} else {
    		System.out.println("Rejected");
    	}
    }
    
    // This routine will be called when A's timer expires (thus generating a 
    // timer interrupt). You'll probably want to use this routine to control 
    // the retransmission of packets. See startTimer() and stopTimer(), above,
    // for how the timer is started and stopped. 
    protected void aTimerInterrupt()
    {
    	System.out.println("A: Timer interrupt. Resending Seq #" + a_seq_base);
    	timeouts++;
    	a_seq_num = a_seq_base;
    	
    	// Imitate a send for each window item.
    	for (int i = 0; i < window.size(); i++)
    	{
			Message message = window.remove();
			if (a_seq_num <= a_seq_max)
    		{
				sends++;
	    		System.out.println("A: Sending " + message.getData() + " With Seq #" + a_seq_num);
		    	Packet sndpkt = new Packet(a_seq_num, 
		    			a_seq_num + 1, 
						(message.getData() + a_seq_num + (a_seq_num + 1)).hashCode(), 
						message.getData());
		    	toLayer3(0, sndpkt);
		    	window.add(message);
	    	if (a_seq_num == a_seq_base)
	    	{
	    		startTimer(0, 30*8);
	    	}
	    		a_seq_num++;
    		}
		}
    }
    
    // This routine will be called once, before any of your other A-side 
    // routines are called. It can be used to do any required
    // initialization (e.g. of member variables you add to control the state
    // of entity A).
    protected void aInit()
    {
    	a_seq_base = 0;
    	a_seq_num = 0;
    	a_seq_max = 7;
    	window = new LinkedList<Message>();
    	
    	message_queue = new LinkedList<Message>();
    	
        sends = 0;
        timeouts = 0;
        receives = 0;
    }
    
    // This routine will be called whenever a packet sent from the B-side 
    // (i.e. as a result of a toLayer3() being done by an A-side procedure)
    // arrives at the B-side.  "packet" is the (possibly corrupted) packet
    // sent from the A-side.
    protected void bInput(Packet packet)
    {
    	Packet sndpkt;
    	System.out.println("B: Receiving Data");
    	
    	// Integrity check and Seqnum check
    	if (packet.getChecksum() == (packet.getPayload() + packet.getSeqnum() + packet.getAcknum()).hashCode()
    			&& packet.getSeqnum() == b_req_num)
    	{
    		successful_receives++;
    		toLayer5(1, packet.getPayload());
    		b_req_num++;
    		
    		System.out.println("B: Received " + packet.getPayload());
    		System.out.println("B: Sending ACK #" + b_req_num);
    	} else {
    		failed_receives++;
    		System.out.println("B: Bad Data Received. Ignoring.");
    	}
    	sndpkt = new Packet(b_req_num - 1, 
    			b_req_num, 
    			("ACK" + (b_req_num - 1) + (b_req_num)).hashCode(), 
    			"ACK");
    	toLayer3(1, sndpkt);
    }
    
    // This routine will be called once, before any of your other B-side 
    // routines are called. It can be used to do any required
    // initialization (e.g. of member variables you add to control the state
    // of entity B).
    protected void bInit()
    {
    	b_req_num = 0;
    	
    	successful_receives = 0;
        failed_receives = 0;
    }
}
