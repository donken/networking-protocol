This should work out-of-box for JRE 1.7. Compile with Java and the input should function from
command line like it did when I received it.

I changed NetworkSimulator.java and StudentNetworkSimulator for this assignment.

Author: David Onken